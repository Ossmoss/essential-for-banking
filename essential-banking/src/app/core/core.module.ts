import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  ApiService 
} from '@core/services';

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [
    ApiService
  ],
})
export class CoreModule { }
