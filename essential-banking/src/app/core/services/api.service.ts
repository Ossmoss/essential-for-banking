import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor() { }

  getData() {
    return {
      name: "Ethereum",
      slug: "ETH",
      pricePerUnit: 100,
      totalUnits: 10,
      totalAmount: 1000,
      info: [
        { 
          title:"About",
          body : "Ethereum is a software system which is part of a  decentralised system meaning it is not controlled  by any single entity. Enthereum is different to Bitcoin because it expands on its technologies to  create a completely new network including an internet browser..." },
        { title:"News"},
        { title:"Statistics" },
        { title:"Positions" },
        { title:"History" }
      ]
    }
  }
}
