import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'buy-modal',
  templateUrl: './buy-modal.component.html',
  styleUrls: ['./buy-modal.component.scss'],
})
export class BuyModalComponent implements OnInit {
  info: any;
  constructor(
    public modalController: ModalController
  ) { }

  ngOnInit() {}
  quantity = 1;


  closeModal() {
    this.modalController.dismiss(false);
  }

  addItem() {
    this.info.totalUnits = this.quantity;
    this.modalController.dismiss(this.info);
  }
}
