import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { BuyModalComponent } from '../buy-modal/buy-modal.component';


@Component({
  selector: 'buy-button',
  templateUrl: './buy-button.component.html',
  styleUrls: ['./buy-button.component.scss'],
})
export class BuyButtonComponent implements OnInit {
  @Input() info: any = {};
  @Output() getInfo: EventEmitter<any> = new EventEmitter();

  constructor(
    public modalController: ModalController
  ) { }

  ngOnInit() { }


  async loadModal() {

    const modal = await this.modalController.create({
      component: BuyModalComponent,
      backdropDismiss: true,
      cssClass: 'my-custom-modal-css',
      componentProps: {
        info: this.info
      }
    });

    modal.onDidDismiss().then(async (data) => {
      console.log(data.data);
      this.getInfo.emit(data.data);


    }).catch(async e => {

    });

    return await modal.present();

  }

}
