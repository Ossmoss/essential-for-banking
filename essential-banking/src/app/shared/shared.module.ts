import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import {
    BuyButtonComponent,
    BuyModalComponent
} from '@shared/components'; 



import { CoreModule } from '@core/module';

@NgModule({
    declarations: [
        // start components
        BuyButtonComponent,
        BuyModalComponent
        // end components

    ],
    entryComponents: [
        BuyModalComponent
    ],
    imports: [
        CommonModule,
        CoreModule,
        IonicModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule
    ],
    exports: [
        BuyButtonComponent,
        BuyModalComponent
        // start pipes
     
        // end pipes
    ]
})

export class SharedModule { }