import { Component, OnInit } from '@angular/core';
import { ApiService } from '@core/services';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(
    public apiSrv: ApiService,
  ) { }
  info: any;

  ngOnInit() {

    this.info = this.apiSrv.getData();

  }

  getInfo(info: any) {
    this.info = info;
  }
}
